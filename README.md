# [stories2](https://gitlab.com/eidoom/stories2)

Previous incarnations: [stories](https://gitlab.com/eidoom/stories), [musings](https://gitlab.com/eidoom/musings).

Initialised with [`create-svelte`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte).

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server and open the app in a new browser tab,

```bash
npm run dev -- --open
```

To expose to network,

```bash
npm run dev -- --host 0.0.0.0
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

## Deploying

### Run from command line

You can deploy the production build with

```shell
HOST=127.0.0.1 \
PORT=3000 \
PROTOCOL_HEADER=x-forwarded-proto \
HOST_HEADER=x-forwarded-host \
node build
```

### Reverse proxy with Nginx

#### Basic

`/etc/nginx/sites-available/stories.conf`

```nginx
server {
    listen 80;

    server_name stories.DOMAIN;

    location / {
        proxy_pass http://127.0.0.1:3000/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

#### Without TLS

To serve without TLS, first edit cookie settings:

`src/routes/admin/+page.server.js`

```diff
     cookies.set("sessionid", session_id, {
       path: "/",
       httpOnly: true,
-      secure: true,
+      secure: false,
       maxAge: 31557600, // 1 year in seconds
+      sameSite: 'lax',
     });
```

#### With TLS

Here's a sample `nginx` proxy server (using [`__tls_wildcard_duckdns`](https://gitlab.com/eidoom/cloud-infrastructure/-/blob/main/type/__tls_wildcard_duckdns/gencode-remote)): `/etc/nginx/sites-available/stories.conf`

```nginx
server {
    listen 80;
    listen [::]:80;

    server_name stories.DOMAIN;

    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name stories.DOMAIN;

    ssl_certificate /etc/letsencrypt/live/star.DOMAIN/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/star.DOMAIN/privkey.pem;

    location / {
        proxy_pass http://127.0.0.1:3000/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

### Systemd service

Create the file,

`/etc/systemd/system/stories.service`

```systemd
[Unit]
Description=Stories
After=network.target

[Service]
User=USER
Group=USER

WorkingDirectory=/PATH/TO/stories2
Environment="HOST=127.0.0.1"
Environment="PORT=3000"
Environment="PROTOCOL_HEADER=x-forwarded-proto"
Environment="HOST_HEADER=x-forwarded-host"
ExecStart=node build

Type=simple
TimeoutStopSec=20
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

and enable and start the service,

```shell
sudo systemctl enable stories.service
sudo systemctl start stories.service
```

After rebuilding after changes, restart the service,

```shell
sudo systemctl restart stories.service
```

## Initialisation

```shell
sudo dnf install sqlite argon2  # fedora
# sudo apt install sqlite3 argon2  # debian
SALT=$(cat /dev/urandom | base64 | head -c 16)
echo -n "<password>" | argon2 $SALT  # output field Encoded is the <hash> (starts with $)
sqlite3 data/session.db
```

```sqlite3
INSERT INTO account (username, password_hash) VALUES ('admin', '<hash>');
```

## Records

### Fonts

#### Text

```shell
mkdir ~/git/stories2/static/fonts/
cd ~/git
git clone git@github.com:SorkinType/Merriweather.git
cd Merriweather/fonts/webfonts/
cp Merriweather-Regular.woff2 Merriweather-Italic.woff2 Merriweather-Bold.woff2 ~/git/stories2/static/fonts/
```

Note that [variable fonts](https://eidoom.gitlab.io/computing-blog/post/zola/#font) are 1024K while above is 232K.
Consider changing to variable fonts only if start using many font variations.

#### Symbols

[Material Icons](https://fonts.google.com/icons?icon.platform=web&icon.style=Rounded&icon.set=Material+Icons)

```shell
git clone git@github.com:google/material-design-icons.git # slow
cd material-design-icons/font/
woff2_compress MaterialIconsRound-Regular.otf
cp MaterialIconsRound-Regular.woff2 ~/git/stories2/static/fonts/
```

## TODO

### UI

-   Metadata sources:
    -   Films https://www.themoviedb.org/documentation/api
    -   TV https://www.tvmaze.com/api#show-main-information
    -   Books https://openlibrary.org/dev/docs/api/books
-   Wordy URL endpoints?
-   Remember page sort/filter config
    - by localStorage?
-   Merge entries and collections index pages?
-   Markdown parser misses emdash `---`
-   Handle crosslinks to other entries within entries
-   Existing short stories should be in separate entries

### DB

-   `medium`/`format`
    -   `medium:book` -> `literature/text`,`comics`,`drawing/image`?
    -   should `format` be an `entry` column?
-   `setting` and `provenance` tags could be merged into `place` tags, but keep distinct taxonomies, distinguished by new relation table column?
    - seems more complicated
    - data would become non-redundant if I added a body to tags
-   Am I happy with `playpause`?
    - There should be an open-ended alternative to the "stop" tyranny, perhaps signified with an open circle (ie outline instead of filled like the record symbol for "active"), for things which don't really have an end, like active websites and books which you don't just read from front to back then discard
-   Use `last_activity` and `created` fields in table `session`
    -   delete if `last_activity` too long ago
    -   delete if `created` > 1 year ago (expiration time of the cookie)
    -   trigger before log in?
    -   also trace device to enforce single session per device?
-   Handle no release year data better for collections
-   Using `continuing` table
-   `original_release` -> timestamp, then can set future release dates
