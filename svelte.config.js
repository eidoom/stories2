import adapter from "@sveltejs/adapter-node";
import * as child_process from "node:child_process";

const config = {
  kit: {
    adapter: adapter(),
    version: {
      // this gives at least 7 characters of hash unique substring (required for GitLab commit links)
      name: child_process.execSync("git rev-parse --short HEAD").toString().trim(),
      pollInterval: 1000,
    },
  },
};

export default config;
