import fs from "fs";
import db from "$lib/server/database";
import dbs from "$lib/server/session";

function load_db_schema(database, schema_file) {
  database.pragma("foreign_keys = ON");

  fs.readFile(schema_file, "utf8", (errFs, schema) => {
    if (errFs) {
      console.error(errFs);
      return;
    }
    try {
      database.exec(schema);
    } catch (errDb) {
      console.error(errDb);
      return;
    }
  });
}

load_db_schema(db, "data/schema-stories.sql");

load_db_schema(dbs, "data/schema-session.sql");
