import db from "$lib/server/database";
import {
  get_way,
  get_sort,
  dateify_list,
  get_pages,
} from "$lib/server/entries";

const sort_map = new Map([
  ["Title", "c.name"],
  ["Count", "count"],
  ["Add date", "c.added"],
  ["Edit date", "c.edited"],
]);

export function load({ url }) {
  const data = url.searchParams;

  const { ways, way } = get_way(data);

  const { sort_keys, sort, sort_col } = get_sort(data, sort_map, 2);

  const number = db.prepare("SELECT COUNT(id) FROM collection").pluck().get();

  const { lim, page, off, pages, start, end } = get_pages(data, number, number);

  const query = `
      SELECT
        c.id,
        c.name,
        c.added,
        c.edited,
        COUNT(e.id) AS count
      FROM entry AS c
      INNER JOIN collection AS a ON a.id = c.id
      LEFT OUTER JOIN entry_collection AS e ON e.collection_id = c.id
      GROUP BY c.id
      ORDER BY ${sort_col} ${way}
      LIMIT ?
      OFFSET ?
      `;

  const collections = db.prepare(query).all([lim, off]);

  dateify_list(collections, ["added", "edited"]);

  return {
    collections,
    page,
    pages,
    lim,
    sort,
    sort_keys,
    way,
    ways,
    start,
    end,
    number,
  };
}
