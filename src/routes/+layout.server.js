import { checkAdmin } from "$lib/server/session";

export async function load({ cookies }) {
  const admin = checkAdmin(cookies);
  return { admin };
}
