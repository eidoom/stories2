import { fail } from "@sveltejs/kit";
import { checkAdmin } from "$lib/server/session";
import db from "$lib/server/database";

export function load() {
  const entries = db
    .prepare(
      `
      SELECT
        m.id,
        m.name,
        COUNT(ef.id) AS count
      FROM tag AS m
      LEFT OUTER JOIN tag_group AS fm ON fm.group_id = m.id
      LEFT OUTER JOIN entry_tag AS ef ON ef.tag_id = fm.member_id
      INNER JOIN taxonomy AS c ON c.id = m.taxonomy_id
      WHERE c.name = 'medium'
      GROUP BY m.id
      ORDER BY m.name
      `,
    )
    .all();

  return { entries };
}

export const actions = {
  new_medium: async ({ cookies, request }) => {
    const admin = checkAdmin(cookies);
    if (!admin) {
      return fail(401, "Unauthorised");
    }

    const transaction = db.transaction((name) => {
      const tax_id = db
        .prepare("SELECT id FROM taxonomy WHERE name = 'medium'")
        .pluck()
        .get();

      db.prepare(
        `
        INSERT INTO tag (name, taxonomy_id)
        VALUES (?, ?)
        `,
      ).run(name, tax_id);
    });

    const name = (await request.formData()).get("name");

    transaction(name);
  },
};
