import { fail } from "@sveltejs/kit";
import { checkAdmin } from "$lib/server/session";
import { db, processCommonFields } from "$lib/server/database";

export function load({ params }) {
  const taxonomy = "medium";

  const taxon = db
    .prepare(
      `
      SELECT id, name, body, added, edited
      FROM tag
      WHERE id = ?
      `,
    )
    .get(params.id);

  if (taxon === undefined) {
    throw error(404, "That tag doesn't exist!");
  }

  const fields = processCommonFields(taxon, "tag");

  const formats = db
    .prepare(
      `
      SELECT
        f.id,
        f.name
      FROM tag AS f
      INNER JOIN tag_group AS tg ON tg.member_id = f.id
      INNER JOIN tag AS m ON m.id = tg.group_id
      WHERE m.id = ?
      `,
    )
    .all(params.id);

  const entries = db
    .prepare(
      `
      SELECT DISTINCT
        e.id,
        e.name,
        c.id IS NOT NULL AS collection
      FROM tag AS m
      INNER JOIN taxonomy AS t ON t.id = m.taxonomy_id
      INNER JOIN tag_group AS fm ON fm.group_id = m.id
      INNER JOIN entry_tag AS ef ON ef.tag_id = fm.member_id
      INNER JOIN entry AS e ON e.id = ef.entry_id
      LEFT OUTER JOIN collection AS c ON c.id = e.id
      WHERE m.id = ?
        AND t.name = ?
      ORDER BY e.name
      `,
    )
    .all(params.id, taxonomy);

  return {
    taxonomy,
    taxon,
    ...fields,
    formats,
    entries,
  };
}

export const actions = {
  rename: async ({ params, cookies, request }) => {
    const admin = checkAdmin(cookies);
    if (!admin) {
      return fail(401, "Unauthorised");
    }

    const name = (await request.formData()).get("name");

    db.prepare("UPDATE tag SET name=? WHERE id = ?").run(name, params.id);
  },
};
