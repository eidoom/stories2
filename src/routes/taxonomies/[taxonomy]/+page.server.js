import db from "$lib/server/database";
import { fail, redirect } from "@sveltejs/kit";
import { checkAdmin } from "$lib/server/session";

export function load({ params }) {
  const entries = db
    .prepare(
      `
        SELECT
          e.id,
          e.name,
          COUNT(r.id) AS count
        FROM tag AS e
        LEFT OUTER JOIN entry_tag AS r ON r.tag_id = e.id
        INNER JOIN taxonomy AS t ON e.taxonomy_id = t.id
        WHERE t.name = ?
        GROUP BY e.id
        ORDER BY e.name
      `,
    )
    .all(params.taxonomy);

  return {
    taxonomy: params.taxonomy,
    entries,
  };
}

export const actions = {
  delete: async ({ params, cookies }) => {
    const admin = checkAdmin(cookies);
    if (!admin) {
      return fail(401, "Unauthorised");
    }

    // TODO move to deleted items table instead, or similar
    try {
      db.prepare("DELETE FROM taxonomy WHERE name = ?").run(params.taxonomy);
    } catch (error) {
      return fail(422, error.message);
    }

    redirect(303, `/taxonomies`);
  },

  rename: async ({ params, cookies, request }) => {
    const admin = checkAdmin(cookies);
    if (!admin) {
      return fail(401, "Unauthorised");
    }

    const name = (await request.formData()).get("taxonomy_name");

    db.prepare("UPDATE taxonomy SET name = ? WHERE name = ?").run(
      name,
      params.taxonomy,
    );

    redirect(303, `/taxonomies/${name}`);
  },
};
