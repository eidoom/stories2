import { db, processCommonFields } from "$lib/server/database";
import { dateify } from "$lib/server/datetime";
import { fail, redirect, error } from "@sveltejs/kit";
import { checkAdmin } from "$lib/server/session";

export function load({ params }) {
  const taxon = db
    .prepare(
      `
      SELECT id, name, body, added, edited
      FROM tag
      WHERE id = ?
      `,
    )
    .get(params.id);

  if (taxon === undefined) {
    error(404, "That tag doesn't exist!");
  }

  const fields = processCommonFields(taxon, "tag");

  const entries = db
    .prepare(
      `
      SELECT
        e.id,
        e.name,
        c.id IS NOT NULL AS collection
      FROM entry AS e
      INNER JOIN entry_tag AS et ON et.entry_id = e.id
      LEFT OUTER JOIN collection AS c ON c.id = e.id
      WHERE et.tag_id = ?
      ORDER BY e.name
      `,
    )
    .all(params.id);

  return {
    taxonomy: params.taxonomy,
    taxon,
    ...fields,
    entries,
  };
}

export const actions = {
  delete: async ({ params, cookies }) => {
    const admin = checkAdmin(cookies);
    if (!admin) {
      return fail(401, "Unauthorised");
    }

    // TODO move to deleted items table instead, or similar
    try {
      db.prepare("DELETE FROM tag WHERE id = ?").run(params.id);
    } catch (error) {
      return fail(422, error.message);
    }

    redirect(303, `/taxonomies/${params.taxonomy}`);
  },
};
