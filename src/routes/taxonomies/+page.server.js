import db from "$lib/server/database";
import { fail } from "@sveltejs/kit";
import { checkAdmin } from "$lib/server/session";

export function load() {
  const taxonomies = db
    .prepare(
      `
        SELECT
          a.name,
          COUNT(b.id) AS count
        FROM taxonomy AS a
        LEFT OUTER JOIN tag AS b ON b.taxonomy_id = a.id
        GROUP BY a.id
        ORDER BY a.name
        `,
    )
    .all();

  return { taxonomies };
}

export const actions = {
  new_taxonomy: async ({ cookies, request }) => {
    const admin = checkAdmin(cookies);
    if (!admin) {
      return fail(401, "Unauthorised");
    }

    const transaction = db.transaction((name) => {
      const info = db
        .prepare(
          `
          INSERT INTO taxonomy (name)
          VALUES (?)
          `,
        )
        .run(name);

      db.prepare(
        `
        INSERT INTO manual_taxonomy (id)
        VALUES (?)
        `,
      ).run(info.lastInsertRowid);
    });

    const name = (await request.formData()).get("taxonomy_name");

    try {
      transaction(name);
    } catch (error) {
      return fail(422, { name, error: error.message });
    }
  },
};
