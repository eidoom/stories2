import { redirect, error } from "@sveltejs/kit";
import { newEntry } from "$lib/server/api";
import { checkAdmin } from "$lib/server/session";

export async function POST({ cookies, request }) {
  const admin = checkAdmin(cookies);
  if (!admin) {
    error(401, "Unauthorised");
  }

  const id = await newEntry(request);

  redirect(303, `/entries/${id}`);
}
