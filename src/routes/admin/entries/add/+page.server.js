import { error } from "@sveltejs/kit";
import {
  all_collections,
  all_tags,
  all_years,
  init_tags,
} from "$lib/server/database";

export async function load({ parent, url }) {
  const { admin } = await parent();
  if (!admin) {
    error(401, "Unauthorised");
  }

  return {
    apidata: {
      name: "",
      version_year: null,
      original_year: null,
      collection: false,
      aliases: [],
      collections: [],
      taxonomies: init_tags(),
      links: [],
      body: "",
      experiences: [],
    },
    url: url.pathname,
    collections: all_collections(),
    taxonomies: all_tags(),
    years: all_years(),
  };
}
