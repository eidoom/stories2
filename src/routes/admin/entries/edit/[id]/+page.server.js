import { error } from "@sveltejs/kit";
import {
  db,
  getBasicEntry,
  init_tags,
  all_collections,
  all_tags,
  all_years,
} from "$lib/server/database";
import { dateString } from "$lib/global";
import { dateify } from "$lib/server/datetime";

export async function load({ parent, params, url }) {
  const { admin } = await parent();
  if (!admin) {
    error(401, "Unauthorised");
  }

  const basic = getBasicEntry(params.id);

  const aliases = db
    .prepare("SELECT id, name FROM entry_alias WHERE entry_id = ?")
    .all(params.id);

  const links = db
    .prepare("SELECT id, url FROM entry_link WHERE entry_id = ?")
    .all(params.id);

  const entry_collections = db
    .prepare(
      `
      SELECT
        e.id,
        c.name,
        o.number
      FROM entry AS c
      INNER JOIN entry_collection AS e ON e.collection_id = c.id
      LEFT OUTER JOIN entry_ordinal AS o ON o.id = e.id
      WHERE e.entry_id = ?
      `
    )
    .all(params.id);

  const entry_taxonomies = init_tags();

  const tmp = db
    .prepare(
      `
      SELECT
        a.name AS tax,
        c.id AS id, -- id of the entry_tag
        b.name AS name
      FROM taxonomy AS a
      INNER JOIN manual_taxonomy AS d ON d.id = a.id
      INNER JOIN tag AS b ON b.taxonomy_id = a.id
      INNER JOIN entry_tag AS c ON c.tag_id = b.id
      WHERE c.entry_id = ?
      `
    )
    .all(params.id);

  for (const { tax, id, name } of tmp) {
    entry_taxonomies[tax].push({ id, name });
  }

  const experience_ids = db
    .prepare("SELECT id FROM experience WHERE entry_id = ?")
    .pluck()
    .all(params.id);

  const experiences = experience_ids.map((experience_id) => {
    const points = db
      .prepare("SELECT id, timestamp FROM playpause WHERE experience_id = ?")
      .all(experience_id)
      .map(({ id, timestamp }) => ({
        id,
        timestamp: dateString(dateify(timestamp)),
      }));

    const complete = !!db
      .prepare("SELECT id FROM complete WHERE id = ?")
      .pluck()
      .get(experience_id);

    const endless = !!db
      .prepare("SELECT id FROM endless WHERE id = ?")
      .pluck()
      .get(experience_id);


    return { id: experience_id, points, complete, endless };
  });

  return {
    apidata: {
      ...basic,
      aliases,
      collections: entry_collections,
      taxonomies: entry_taxonomies,
      links,
      experiences,
    },
    url: url.pathname,
    collections: all_collections(),
    taxonomies: all_tags(),
    years: all_years(),
  };
}
