import { redirect, error } from "@sveltejs/kit";
import db from "$lib/server/database";
import { editEntryMeta } from "$lib/server/api";
import { checkAdmin } from "$lib/server/session";

export async function POST({ params, cookies, request }) {
  const admin = checkAdmin(cookies);
  if (!admin) {
    error(401, "Unauthorised");
  }

  const transaction = db.transaction((entry_id, data) => {
    db.prepare(
      `
      UPDATE entry
      SET name=?, body=?, edited=UNIXEPOCH()
      WHERE id = ?
      `,
    ).run(data.name, data.body, entry_id);

    editEntryMeta(entry_id, data);

    return entry_id;
  });

  const data = await request.json();

  transaction(params.id, data);

  redirect(303, `/entries/${params.id}`);
}
