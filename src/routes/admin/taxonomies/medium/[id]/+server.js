import { redirect, error } from "@sveltejs/kit";
import db from "$lib/server/database";
import { getIds, getQs } from "$lib/server/api";
import { checkAdmin } from "$lib/server/session";

export async function POST({ params, cookies, request }) {
  const admin = checkAdmin(cookies);
  if (!admin) {
    error(401, "Unauthorised");
  }

  const transaction = db.transaction((medium_id, data) => {
    const tag_group_ids = getIds(data);

    db.prepare(
      `
      DELETE FROM tag_group
      WHERE
        group_id = ?
        AND
        id NOT IN (${getQs(tag_group_ids)})
      `,
    ).run(medium_id, ...tag_group_ids);

    const tax_id = db
      .prepare("SELECT id FROM taxonomy WHERE name = 'format'")
      .pluck()
      .get();

    for (const medium_format of data) {
      db.prepare(
        `INSERT OR IGNORE INTO tag (name, taxonomy_id) VALUES (?, ?)`,
      ).run(medium_format.name, tax_id);

      const format_id = db
        .prepare(`SELECT id FROM tag WHERE name = ? AND taxonomy_id = ?`)
        .pluck()
        .get(medium_format.name, tax_id);

      if (medium_format.hasOwnProperty("id")) {
        db.prepare(
          `
          UPDATE tag_group
          SET
            member_id = ?,
            group_id = ?
          WHERE id = ?
          `,
        ).run(format_id, medium_id, medium_format.id);
      } else {
        db.prepare(
          `INSERT INTO tag_group (member_id, group_id) VALUES (?, ?)`,
        ).run(format_id, medium_id);
      }
    }
  });

  const data = await request.json();

  try {
    transaction(params.id, data);
  } catch (err) {
    error(422, err.message);
  }

  redirect(303, `/taxonomies/medium/${params.id}`);
}
