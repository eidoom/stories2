import db from "$lib/server/database";

export async function load({ parent, params, url }) {
  const { admin } = await parent();
  if (!admin) {
    throw error(401, "Unauthorised");
  }

  const taxon = db
    .prepare("SELECT name FROM tag WHERE id = ?")
    .pluck()
    .get(params.id);

  const current = db
    .prepare(
      `
      SELECT
        tg.id,
        f.name
      FROM tag AS f
      INNER JOIN tag_group AS tg ON tg.member_id = f.id
      INNER JOIN tag AS m ON m.id = tg.group_id
      WHERE m.id = ?
      `,
    )
    .all(params.id);

  const unused = db
    .prepare(
      `
      SELECT
        f.name
      FROM tag AS f
      INNER JOIN taxonomy AS t ON f.taxonomy_id = t.id
      LEFT OUTER JOIN tag_group AS tg ON tg.member_id = f.id
      WHERE tg.id IS NULL AND t.name = 'format'
      `,
    )
    .pluck()
    .all();

  return {
    taxon,
    apidata: current,
    unused,
    url: url.pathname,
  };
}
