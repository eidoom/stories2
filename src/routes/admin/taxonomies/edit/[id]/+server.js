import { redirect, error } from "@sveltejs/kit";
import db from "$lib/server/database";
import { editCommon } from "$lib/server/api";
import { checkAdmin } from "$lib/server/session";

export async function POST({ params, cookies, request }) {
  const admin = checkAdmin(cookies);
  if (!admin) {
    error(401, "Unauthorised");
  }

  const transaction = db.transaction((data) => {
    db.prepare(
      `
      UPDATE tag
      SET name=?,
          body=?,
          edited=UNIXEPOCH()
      WHERE id = ?
      `,
    ).run(data.name, data.body, data.id);

    editCommon(data.id, data, "tag");
  });

  const data = await request.json();

  try {
    transaction(data);
  } catch (err) {
    error(422, err.message);
  }

  redirect(303, `/taxonomies/${data.taxonomy}/${data.id}`);
}
