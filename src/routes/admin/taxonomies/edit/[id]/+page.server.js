import { error, fail, redirect } from "@sveltejs/kit";
import { checkAdmin } from "$lib/server/session";
import db from "$lib/server/database";
import { dateString } from "$lib/global";
import { dateify } from "$lib/server/datetime";

export async function load({ parent, params, url }) {
  const { admin } = await parent();
  if (!admin) {
    error(401, "Unauthorised");
  }

  const taxon = db
    .prepare(
      `
      SELECT
        t.id,
        t.name,
        t.body,
        t.added,
        t.edited,
        t.taxonomy_id,
        u.name AS taxonomy
      FROM tag AS t
      INNER JOIN taxonomy AS u ON u.id = t.taxonomy_id
      WHERE t.id = ?
      `,
    )
    .get(params.id);

  const aliases = db
    .prepare("SELECT id, name FROM tag_alias WHERE tag_id = ?")
    .all(params.id);

  const links = db
    .prepare("SELECT id, url FROM tag_link WHERE tag_id = ?")
    .all(params.id);

  const other_tags = db
    .prepare(
      `
      SELECT name
      FROM tag
      WHERE taxonomy_id = ?
      `,
    )
    .pluck()
    .all(taxon.taxonomy_id);

  const all_taxonomies = db
    .prepare(
      `
      SELECT name
      FROM taxonomy
      `,
    )
    .pluck()
    .all();

  return {
    apidata: {
      ...taxon,
      aliases,
      links,
    },
    other_tags,
    all_taxonomies,
    url: url.pathname,
  };
}

export const actions = {
  merge: async ({ params, cookies, request }) => {
    const admin = checkAdmin(cookies);
    if (!admin) {
      return fail(401, "Unauthorised");
    }

    // merge tag 'this' into tag 'other'
    const transaction = db.transaction((this_id, other_name) => {
      // get 'other' id
      const other_id = db
        .prepare("SELECT id FROM tag WHERE name = ?")
        .pluck()
        .get(other_name);

      // delete entry-tag relations for tag 'this' where the entry already has tag 'other'
      db.prepare(
        `
        DELETE FROM entry_tag
        WHERE id IN (
          SELECT t1.id FROM entry_tag AS t1
          INNER JOIN entry AS e ON e.id = t1.entry_id
          INNER JOIN entry_tag AS t2 ON e.id = t2.entry_id
          WHERE t2.tag_id = ? AND t1.tag_id = ?
        )
        `,
      ).run(other_id, this_id);

      // change remaining entry-tag relations for 'this' to 'other'
      db.prepare(
        `
        UPDATE entry_tag
        SET tag_id = ?
        WHERE tag_id = ?
        `,
      ).run(other_id, this_id);

      // add 'this' name as alias of 'other'
      db.prepare(
        `
        INSERT INTO tag_alias (tag_id, name)
        VALUES (?, (SELECT name FROM tag WHERE id = ?))
        `,
      ).run(other_id, this_id);

      // change 'this' tag aliases to be for 'other'
      db.prepare(
        `
        UPDATE tag_alias
        SET tag_id = ?
        WHERE tag_id = ?
        `,
      ).run(other_id, this_id);

      // change 'this' tag links to be for 'other'
      db.prepare(
        `
        UPDATE tag_link
        SET tag_id = ?
        WHERE tag_id = ?
        `,
      ).run(other_id, this_id);

      // update 'edited' and
      // use the oldest 'added' date from 'this' and 'other' for 'other' and
      // append 'this' body to 'other' if 'this' body non-empty
      db.prepare(
        `
        UPDATE tag
        SET edited = UNIXEPOCH(),
            added = MIN(tag.added, old.added),
            body = tag.body || IIF(old.body == '', '', '\n' || old.body)
        FROM tag AS old
        WHERE tag.id = ? AND old.id = ?
        `,
      ).run(other_id, this_id);

      // delete 'this' tag
      db.prepare("DELETE FROM tag WHERE id = ?").run(this_id);

      return other_id;
    });

    const data = await request.formData();
    const other_tag = data.get("other-tag");

    const new_id = transaction(params.id, other_tag);

    // nb if 'this' taxonomy was different to 'other', it is ignored and 'other' keeps its original taxonomy
    const taxonomy = db
      .prepare(
        `
        SELECT u.name
        FROM tag AS t
        INNER JOIN taxonomy AS u ON t.taxonomy_id = u.id
        WHERE t.id = ?
        `,
      )
      .pluck()
      .get(new_id);

    redirect(303, `/taxonomies/${taxonomy}/${new_id}`);
  },
};
