import argon2 from "argon2";
import { fail } from "@sveltejs/kit";
import { dbs, createSession } from "$lib/server/session";
import { dateify } from "$lib/server/datetime";

export async function load({ parent, cookies }) {
  const { admin } = await parent();

  if (admin) {
    const session_id = cookies.get("sessionid");

    const { created, last_activity } = dbs
      .prepare(
        `
      SELECT
        created,
        last_activity
      FROM session
      WHERE session_id = ?
      `,
      )
      .get(session_id);

    return { created: dateify(created), last_activity: dateify(last_activity) };
  }
}

export const actions = {
  login: async ({ cookies, request }) => {
    const username = "admin";

    const data = await request.formData();
    const password = data.get("password");

    const { account_id, password_hash } = dbs
      .prepare(
        "SELECT id AS account_id, password_hash FROM account WHERE username = ?",
      )
      .get(username);

    if (!(await argon2.verify(password_hash, password))) {
      return fail(400, { incorrect_password: true });
    }

    const session_id = createSession();

    cookies.set("sessionid", session_id, {
      path: "/",
      httpOnly: true,
      secure: true,
      maxAge: 31557600, // 1 year in seconds
    });

    dbs
      .prepare("INSERT INTO session (session_id, account_id) VALUES (?, ?)")
      .run(session_id, account_id);

    return { login_success: true };
  },

  logout: async ({ cookies }) => {
    const session_id = cookies.get("sessionid");

    if (session_id === undefined) {
      return fail(401); // Already logged out
    }

    cookies.delete("sessionid", { path: "/" });

    dbs.prepare("DELETE FROM session WHERE session_id = ?").run(session_id);

    return { logout_success: true };
  },
};
