import { error } from "@sveltejs/kit";
import db from "$lib/server/database";
import {
  get_way,
  get_sort,
  dateify_list,
  get_pages,
} from "$lib/server/entries";

const sort_map = new Map([
  ["Title", "e.name"],
  ["Add date", "e.added"],
  ["Edit date", "e.edited"],
  ["Release year", "year"],
  ["Personal premiere", "started"],
  ["Last experienced", "last"],
]);

const allowed_statuses = [
  "active",
  "endless",
  "completed",
  "experienced",
  "discussed",
  "released",
];

export function load({ url }) {
  const data = url.searchParams;

  const { ways, way } = get_way(data);

  const { sort_keys, sort, sort_col } = get_sort(data, sort_map, 4);

  const status_filters = {};
  for (const status of allowed_statuses) {
    const inex = [`${status}_inc`, `${status}_exc`];

    if (inex.every((n) => data.has(n))) {
      error(422, `${inex[0]} and ${inex[1]} are mutually exclusive`);
    }

    for (const name of inex) {
      status_filters[name] = data.has(name);
    }
  }

  const filter_medium = data.get("filter_medium") ?? "";
  const media = [
    ...db
      .prepare(
        "SELECT a.name FROM tag AS a INNER JOIN taxonomy AS b ON b.id = a.taxonomy_id WHERE b.name = 'medium'",
      )
      .pluck()
      .all(),
    "",
  ];
  if (!media.includes(filter_medium)) {
    error(
            422,
            `You sent '${filter_medium}' for filter_medium, which must be one of ${media.join(
              ", ",
            )}`,
          );
  }

  const filter_tags = data.getAll("filter_tag");
  if (filter_tags.length == 0) {
    filter_tags.push("");
  }
  const tags = db.prepare("SELECT name FROM tag ORDER BY name").pluck().all();

  const filter_year_experienced = data.get("filter_year_experienced") ?? "";
  const years_experienced = db
    .prepare(
      `
    SELECT DISTINCT strftime('%Y', MIN(p.timestamp), 'unixepoch') AS year
    FROM experience AS e
    INNER JOIN playpause AS p ON e.id = p.experience_id
    GROUP BY e.id
    ORDER BY year;
    `,
    )
    .pluck()
    .all();

  const max_num = db
    .prepare(
      `
      SELECT COUNT(e.id)
      FROM entry AS e
      LEFT OUTER JOIN collection AS c ON c.id = e.id
      WHERE c.id IS NULL
      `,
    )
    .pluck()
    .get();

  let query = `
      SELECT
        e.id,
        e.name,
        e.added,
        e.edited,
        IFNULL(y.name, 'unreleased') AS year,
        IFNULL(GROUP_CONCAT(DISTINCT m.name), '-') AS media,
        MIN(p.timestamp) AS started,
        MAX(p.timestamp) AS last,
        x.id IS NOT NULL AS experienced,
        LENGTH(e.body) > 50 AS discussed,
        c.id IS NOT NULL AS completed,
        l.id IS NOT NULL AS endless,
        a.id IS NOT NULL AS released,
        COUNT(DISTINCT p.id) % 2 AND c.id IS NULL AND l.id IS NULL AS active
      FROM entry AS e
      LEFT OUTER JOIN collection AS z ON z.id = e.id
      LEFT OUTER JOIN original_release AS a ON a.id = e.id
      LEFT OUTER JOIN year AS y ON y.id = a.year_id
      LEFT OUTER JOIN entry_tag AS b ON b.entry_id = e.id
      LEFT OUTER JOIN tag_group AS d ON d.member_id = b.tag_id
      LEFT OUTER JOIN tag AS m ON m.id = d.group_id
      LEFT OUTER JOIN taxonomy AS c1 ON c1.id = m.taxonomy_id
      LEFT OUTER JOIN experience as x ON x.entry_id = e.id
      LEFT OUTER JOIN playpause AS p ON p.experience_id = x.id
      LEFT OUTER JOIN complete AS c ON c.id = x.id
      LEFT OUTER JOIN endless AS l ON l.id = x.id
      `;

  for (const i in filter_tags) {
    const filter_tag = filter_tags[i];
    if (filter_tag != "") {
      query += `
      LEFT OUTER JOIN entry_tag AS et${i} ON et${i}.entry_id = e.id
      LEFT OUTER JOIN tag AS t${i} ON t${i}.id = et${i}.tag_id
    `;
    }
  }

  query += `
      WHERE (c1.name = 'medium' OR c1.name IS NULL)
      AND z.id IS NULL
      `;

  let args = [];

  if (filter_medium != "") {
    query += "AND m.name = ?";
    args.push(filter_medium);
  }

  for (const i in filter_tags) {
    const filter_tag = filter_tags[i];
    if (filter_tag != "") {
      query += `AND t${i}.name = ?`;
      args.push(filter_tag);
    }
  }

  query += `
      GROUP BY e.id
      `;

  let j = 0;
  for (const status of allowed_statuses) {
    if (status_filters[`${status}_inc`]) {
      query += j === 0 ? "HAVING" : " AND";
      query += ` ${status} = 1`;
      j += 1;
    } else if (status_filters[`${status}_exc`]) {
      query += j === 0 ? "HAVING" : " AND";
      query += ` ${status} = 0`;
      j += 1;
    }
  }

  if (filter_year_experienced != "") {
    query += j === 0 ? "HAVING" : " AND";
    query += " strftime('%Y', started, 'unixepoch') = ?";
    args.push(filter_year_experienced);
    j += 1;
  }

  const number = db
    .prepare(`WITH q AS (${query}) SELECT COUNT(q.id) FROM q`)
    .pluck()
    .get(args);

  const { lim, page, off, pages } = get_pages(data, max_num, number);

  args.push(lim, off);

  const paginated_query = `
      ${query}
      ORDER BY ${sort_col} ${way}
      LIMIT ?
      OFFSET ?
      `;

  const entries = db.prepare(paginated_query).all(args);

  dateify_list(entries, ["added", "edited", "started", "last"]);

  for (const [i, entry] of entries.entries()) {
    entries[i].media = entry.media.replace(",", "/");
  }

  return {
    entries,
    max_num,
    number,
    page,
    lim,
    pages,
    sort,
    sort_keys,
    way,
    ways,
    allowed_statuses,
    status_filters,
    media,
    filter_medium,
    tags,
    filter_tags,
    years_experienced,
    filter_year_experienced,
  };
}
