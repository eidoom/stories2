import { fail, redirect } from "@sveltejs/kit";
import { db, processCommonFields } from "$lib/server/database";
import { checkAdmin } from "$lib/server/session";
import { dateify } from "$lib/server/datetime";

function subentries(entry, depth = 0) {
  if (depth > 2 || !entry.collection) return [];

  const entries = db
    .prepare(
      `
      SELECT
        e.id,
        e.name,
        o.number,
        IFNULL(y.name, 'unreleased') AS year,
        IFNULL(GROUP_CONCAT(DISTINCT m.name), '-') AS media,
        r.id IS NOT NULL AS collection
      FROM entry AS e
      INNER JOIN entry_collection AS c ON c.entry_id = e.id
      LEFT OUTER JOIN entry_ordinal AS o ON o.id = c.id
      LEFT OUTER JOIN original_release AS a ON a.id = e.id
      LEFT OUTER JOIN year AS y ON y.id = a.year_id
      LEFT OUTER JOIN entry_tag AS b ON b.entry_id = e.id
      LEFT OUTER JOIN tag_group AS d ON d.member_id = b.tag_id
      LEFT OUTER JOIN tag AS m ON m.id = d.group_id
      LEFT OUTER JOIN taxonomy AS c1 ON c1.id = m.taxonomy_id
      LEFT OUTER JOIN collection AS r ON r.id = e.id
      WHERE c.collection_id = ?
        AND (c1.name = 'medium' OR c1.name IS NULL)
      GROUP BY e.id
      ORDER BY o.number
      `,
    )
    .all(entry.id);

  for (const [i, subentry] of entries.entries()) {
    entries[i].entries = subentries(subentry, depth + 1);
  }

  return entries;
}

export function load({ params }) {
  const entry = db
    .prepare(
      `
      SELECT
        e.id,
        e.name,
        e.added,
        e.edited,
        e.body,
        IFNULL(oy.name, 'unreleased') AS original_year,
        ry.name AS release_year,
        c.id IS NOT NULL AS collection
      FROM entry AS e
      LEFT OUTER JOIN version_release AS r ON r.id = e.id
      LEFT OUTER JOIN year AS ry ON ry.id = r.year_id
      LEFT OUTER JOIN original_release AS o ON o.id = e.id
      LEFT OUTER JOIN year AS oy ON oy.id = o.year_id
      LEFT OUTER JOIN collection AS c ON c.id = e.id
      WHERE e.id = ?
      `,
    )
    .get(params.id);

  const fields = processCommonFields(entry, "entry");

  const collections = db
    .prepare(
      `
      SELECT
        c.id,
        c.name,
        o.number
      FROM entry AS c
      INNER JOIN entry_collection AS e ON e.collection_id = c.id
      LEFT OUTER JOIN entry_ordinal AS o ON o.id = e.id
      WHERE e.entry_id = ?
      `,
    )
    .all(params.id);

  // get manual tags
  const tmp = db
    .prepare(
      `
      SELECT
        a.name AS tax,
        b.id AS id, -- id of the tag
        b.name AS tag
      FROM taxonomy AS a
      INNER JOIN manual_taxonomy AS d ON d.id = a.id
      INNER JOIN tag AS b ON b.taxonomy_id = a.id
      INNER JOIN entry_tag AS c ON c.tag_id = b.id
      WHERE c.entry_id = ?
      `,
    )
    .all(params.id);

  // nest tag data by taxonomy
  const taxonomies = {};
  for (const t of tmp) {
    if (!taxonomies.hasOwnProperty(t.tax)) {
      taxonomies[t.tax] = [];
    }
    taxonomies[t.tax].push({ id: t.id, name: t.tag });
  }

  taxonomies["medium"] = db
    .prepare(
      `
      SELECT DISTINCT
        m.id,
        m.name
      FROM tag AS m
      INNER JOIN tag_group AS fm ON fm.group_id = m.id
      INNER JOIN entry_tag AS ef ON ef.tag_id = fm.member_id
      INNER JOIN taxonomy AS c ON c.id = m.taxonomy_id
      WHERE ef.entry_id = ?
      AND c.name = 'medium'
      `,
    )
    .all(params.id);

  const experiences = db
    .prepare(
      `
      SELECT
        MIN(timestamp) AS start,
        IIF( COUNT(p.timestamp) % 2 == 0 AND COUNT(p.timestamp) > 1, MAX(p.timestamp), NULL ) AS end,
        COUNT(DISTINCT p.id) % 2 AND c.id IS NULL AND l.id IS NULL AS active,
        c.id IS NOT NULL AS complete,
        l.id IS NOT NULL AS endless
      FROM experience AS x
      LEFT OUTER JOIN playpause AS p ON p.experience_id = x.id
      LEFT OUTER JOIN complete AS c ON c.id = x.id
      LEFT OUTER JOIN endless AS l ON l.id = x.id
      WHERE x.entry_id = ?
      GROUP BY x.id
      `,
    )
    .all(params.id);

  for (const [i, experience] of experiences.entries()) {
    for (const point of ["start", "end"]) {
      if (experience[point] !== null) {
        experiences[i][point] = dateify(experience[point]);
      }
    }
  }

  const entries = subentries(entry);

  return {
    entry,
    ...fields,
    collections,
    taxonomies,
    experiences,
    entries,
  };
}

export const actions = {
  delete: async ({ params, cookies }) => {
    const admin = checkAdmin(cookies);
    if (!admin) {
      return fail(401, "Unauthorised");
    }

    // TODO move to deleted items table instead, or similar
    db.prepare("DELETE FROM entry WHERE id = ?").run(params.id);

    redirect(303, "/entries");
  },

  new_experience: async ({ params, cookies }) => {
    const admin = checkAdmin(cookies);
    if (!admin) {
      return fail(401, "Unauthorised");
    }

    const transaction = db.transaction((entry_id) => {
      db.prepare("UPDATE entry SET edited = UNIXEPOCH() WHERE id = ?").run(
        entry_id,
      );

      const experience_id = db
        .prepare("INSERT INTO experience (entry_id) VALUES (?)")
        .run(entry_id).lastInsertRowid;

      db.prepare("INSERT INTO playpause (experience_id) VALUES (?)").run(
        experience_id,
      );
    });

    transaction(params.id);
  },

  new_done_experience: async ({ params, cookies }) => {
    const admin = checkAdmin(cookies);
    if (!admin) {
      return fail(401, "Unauthorised");
    }

    const transaction = db.transaction((entry_id) => {
      db.prepare("UPDATE entry SET edited = UNIXEPOCH() WHERE id = ?").run(
        entry_id,
      );

      const experience_id = db
        .prepare("INSERT INTO experience (entry_id) VALUES (?)")
        .run(entry_id).lastInsertRowid;

      for (let i = 0; i < 2; i++) {
        db.prepare("INSERT INTO playpause (experience_id) VALUES (?)").run(
          experience_id,
        );
      }

      db.prepare("INSERT INTO complete (id) VALUES (?)").run(experience_id);
    });

    transaction(params.id);
  },

  experience_playpause: async ({ params, cookies }) => {
    const admin = checkAdmin(cookies);
    if (!admin) {
      return fail(401, "Unauthorised");
    }

    const transaction = db.transaction((entry_id) => {
      db.prepare("UPDATE entry SET edited = UNIXEPOCH() WHERE id = ?").run(
        entry_id,
      );

      const experience_id = db
        .prepare(
          "SELECT id FROM experience WHERE entry_id = ? ORDER BY id DESC LIMIT 1",
        )
        .pluck()
        .get(entry_id);

      db.prepare("INSERT INTO playpause (experience_id) VALUES (?)").run(
        experience_id,
      );
    });

    transaction(params.id);
  },

  experience_stop: async ({ params, cookies }) => {
    const admin = checkAdmin(cookies);
    if (!admin) {
      return fail(401, "Unauthorised");
    }

    const transaction = db.transaction((entry_id) => {
      db.prepare("UPDATE entry SET edited = UNIXEPOCH() WHERE id = ?").run(
        entry_id,
      );

      const experience_id = db
        .prepare(
          "SELECT id FROM experience WHERE entry_id = ? ORDER BY id DESC LIMIT 1",
        )
        .pluck()
        .get(entry_id);

      db.prepare("INSERT INTO playpause (experience_id) VALUES (?)").run(
        experience_id,
      );

      db.prepare("INSERT INTO complete (id) VALUES (?)").run(experience_id);
    });

    transaction(params.id);
  },
};
