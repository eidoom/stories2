import { error } from "@sveltejs/kit";
import db from "$lib/server/database";
import { get_pages, show_default, page_default } from "$lib/server/entries";

export function load({ url }) {
  const types = ["entry", "meta"],
    default_type = types[0],
    sp = url.searchParams,
    path = url.pathname,
    type = sp.get("type") ?? default_type,
    search = sp.get("search") ?? "";

  let query,
    results = null,
    number = 0,
    lim = show_default,
    page = page_default,
    pages = 1,
    off = 0;

  if (search !== "") {
    switch (type) {
      case "entry":
        // TODO include aliases
        query = `
            SELECT
              r.id,
              r.name,
              IFNULL(y.name, 'unreleased') AS year,
              IFNULL(GROUP_CONCAT(DISTINCT m.name), '-') AS media,
              c.id IS NOT NULL AS collection
            FROM entry AS r
            LEFT OUTER JOIN collection AS c ON c.id = r.id
            LEFT OUTER JOIN original_release AS a ON a.id = r.id
            LEFT OUTER JOIN year AS y ON y.id = a.year_id
            LEFT OUTER JOIN entry_tag AS b ON b.entry_id = r.id
            LEFT OUTER JOIN tag_group AS d ON d.member_id = b.tag_id
            LEFT OUTER JOIN tag AS m ON m.id = d.group_id
            LEFT OUTER JOIN taxonomy AS c1 ON c1.id = m.taxonomy_id
            WHERE r.name LIKE ?
              AND (c1.name = 'medium' OR c1.name IS NULL)
            GROUP BY r.id
            `;
        break;

      case "meta":
        query = `
            SELECT
              r.id,
              r.name,
              'taxonomies/' || b.name AS root
            FROM tag AS r
            INNER JOIN taxonomy AS b ON b.id = r.taxonomy_id
            WHERE r.name LIKE ?
            `;
        break;

      default:
        error(
                      422,
                      `You sent '${type}' for type, which must be one of ${types}`,
                    );
    }

    number = db
      .prepare(`SELECT COUNT(id) FROM (${query})`)
      .pluck()
      .get(`%${search}%`);

    ({ lim, page, off, pages } = get_pages(sp, number, number));

    results = db
      .prepare(
        `
        ${query}
        ORDER BY r.name
        LIMIT ?
        OFFSET ?
        `,
      )
      .all(`%${search}%`, lim, off);
  }

  return {
    static_type: type,
    type,
    types,
    static_search: search,
    search,
    results,
    path,
    lim,
    page,
    pages,
    number,
    default_type,
    show_default,
    page_default,
  };
}
