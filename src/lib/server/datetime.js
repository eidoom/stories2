// sqlite uses seconds while js uses milliseconds for timestamp

export function dateify(stamp) {
  return new Date(1000 * stamp);
}

export function stampify(date) {
  return Math.floor(new Date(date).getTime() / 1000);
}
