import { redirect } from "@sveltejs/kit";
import db from "$lib/server/database";
import { stampify } from "$lib/server/datetime";

export function getIds(datum) {
  return datum.map((o) => o.id).filter((i) => i !== undefined);
}

export function getQs(list) {
  return Array(list.length).fill("?").join(",");
}

export function editCommon(entry_id, data, kind) {
  // aliases

  const alias_ids = getIds(data.aliases);
  db.prepare(
    `
    DELETE FROM ${kind}_alias
    WHERE ${kind}_id = ?
      AND id NOT IN (${getQs(alias_ids)})
    `,
  ).run(entry_id, ...alias_ids);

  for (const alias of data.aliases) {
    if (alias.hasOwnProperty("id")) {
      db.prepare(`UPDATE ${kind}_alias SET name = ? WHERE id = ?`).run(
        alias.name,
        alias.id,
      );
    } else {
      db.prepare(
        `INSERT INTO ${kind}_alias (${kind}_id, name) VALUES (?, ?)`,
      ).run(entry_id, alias.name);
    }
  }

  // links

  const link_ids = getIds(data.links);
  db.prepare(
    `
    DELETE FROM ${kind}_link
    WHERE ${kind}_id = ?
      AND id NOT IN (${getQs(link_ids)})
    `,
  ).run(entry_id, ...link_ids);

  for (const link of data.links) {
    if (link.hasOwnProperty("id")) {
      db.prepare(`UPDATE ${kind}_link SET url = ? WHERE id = ?`).run(
        link.url,
        link.id,
      );
    } else {
      db.prepare(
        `INSERT INTO ${kind}_link (${kind}_id, url) VALUES (?, ?)`,
      ).run(entry_id, link.url);
    }
  }
}

export function editEntryMeta(entry_id, data) {
  // collection checkbox
  if (data.collection) {
    db.prepare("INSERT OR IGNORE INTO collection (id) VALUES (?)").run(
      entry_id,
    );
  } else {
    db.prepare(`DELETE FROM collection WHERE id = ?`).run(entry_id);
  }

  // release years
  for (const year of ["version", "original"]) {
    if (data[`${year}_year`] === null) {
      db.prepare(`DELETE FROM ${year}_release WHERE id = ?`).run(entry_id);
    } else {
      db.prepare(`INSERT OR IGNORE INTO year (name) VALUES (?)`).run(
        data[`${year}_year`],
      );

      const year_id = db
        .prepare(`SELECT id FROM year WHERE name = ?`)
        .pluck()
        .get(data[`${year}_year`]);

      db.prepare(
        `
        INSERT INTO ${year}_release (id, year_id)
        VALUES (@entry_id, @year_id)
        ON CONFLICT (id)
        DO UPDATE SET year_id = @year_id
        `,
      ).run({ entry_id, year_id });
    }
  }

  editCommon(entry_id, data, "entry");

  // collections

  const collection_ids = getIds(data.collections);
  db.prepare(
    `
    DELETE FROM entry_collection
    WHERE entry_id = ?
      AND id NOT IN (${getQs(collection_ids)})
    `,
  ).run(entry_id, ...collection_ids);

  for (const collection of data.collections) {
    // TODO What if name already exists as a non-collection but I want to upgrade that entry to a collection rather than make a new collection entry? Currently have to first manually upgrade that entry to a collection.
    // TODO Currently no checks on duplicate collection names. This will always select the first one (the one with lower id).
    let collection_id = db
      .prepare(
        `
        SELECT e.id
        FROM entry AS e
        INNER JOIN collection AS c ON c.id = e.id
        WHERE e.name = ?
        `,
      )
      .pluck()
      .get(collection.name);

    if (collection_id === undefined) {
      collection_id = db
        .prepare("INSERT INTO entry (name) VALUES (?)")
        .run(collection.name).lastInsertRowid;

      db.prepare("INSERT INTO collection (id) VALUES (?)").run(collection_id);
    }

    let entry_collection_id;
    if (collection.hasOwnProperty("id")) {
      entry_collection_id = collection.id;

      db.prepare(
        "UPDATE entry_collection SET collection_id = ? WHERE id = ?",
      ).run(collection_id, entry_collection_id);
    } else {
      entry_collection_id = db
        .prepare(
          "INSERT INTO entry_collection (entry_id, collection_id) VALUES (?, ?)",
        )
        .run(entry_id, collection_id).lastInsertRowid;
    }

    if (collection.hasOwnProperty("number")) {
      if (collection.number === null) {
        db.prepare("DELETE FROM entry_ordinal WHERE id = ?").run(
          entry_collection_id,
        );
      } else {
        db.prepare(
          `
          INSERT INTO entry_ordinal (id, number) VALUES (@id, @num)
            ON CONFLICT (id) DO UPDATE SET number = @num
          `,
        ).run({ id: entry_collection_id, num: collection.number });
      }
    }
  }

  // taxonomies

  const entry_tag_ids = getIds(Object.values(data.taxonomies).flat());
  db.prepare(
    `
    DELETE FROM entry_tag
    WHERE entry_id = ?
      AND id NOT IN (${getQs(entry_tag_ids)})
    `,
  ).run(entry_id, ...entry_tag_ids);

  for (const [tax, entry_tags] of Object.entries(data.taxonomies)) {
    const tax_id = db
      .prepare("SELECT id FROM taxonomy WHERE name = ?")
      .pluck()
      .get(tax);

    for (const entry_tag of entry_tags) {
      db.prepare(
        `INSERT OR IGNORE INTO tag (name, taxonomy_id) VALUES (?, ?)`,
      ).run(entry_tag.name, tax_id);

      const tag_id = db
        .prepare(`SELECT id FROM tag WHERE name = ? AND taxonomy_id = ?`)
        .pluck()
        .get(entry_tag.name, tax_id);

      if (entry_tag.hasOwnProperty("id")) {
        db.prepare(`UPDATE entry_tag SET tag_id = ? WHERE id = ?`).run(
          tag_id,
          entry_tag.id,
        );
      } else {
        db.prepare(
          `INSERT INTO entry_tag (entry_id, tag_id) VALUES (?, ?)`,
        ).run(entry_id, tag_id);
      }
    }
  }

  // experiences

  const experience_ids = getIds(data.experiences);
  db.prepare(
    `
    DELETE FROM experience
    WHERE entry_id = ?
      AND id NOT IN (${getQs(experience_ids)})
    `,
  ).run(entry_id, ...experience_ids);

  for (const experience of data.experiences) {
    const experience_id = experience.hasOwnProperty("id")
      ? experience.id
      : db.prepare("INSERT INTO experience (entry_id) VALUES (?)").run(entry_id)
          .lastInsertRowid;

    const point_ids = getIds(experience.points);
    db.prepare(
      `
      DELETE FROM playpause
      WHERE experience_id = ?
        AND id NOT IN (${getQs(point_ids)})
      `,
    ).run(experience_id, ...point_ids);

    for (const point of experience.points) {
      const timestamp = stampify(point.timestamp);

      if (point.hasOwnProperty("id")) {
        db.prepare(`UPDATE playpause SET timestamp = ? WHERE id = ?`).run(
          timestamp,
          point.id,
        );
      } else {
        db.prepare(
          "INSERT INTO playpause (experience_id, timestamp) VALUES (?, ?)",
        ).run(experience_id, timestamp);
      }
    }

    if (experience.hasOwnProperty("complete")) {
      if (!experience.complete) {
        db.prepare("DELETE FROM complete WHERE id = ?").run(experience_id);
      } else {
        db.prepare("INSERT OR IGNORE INTO complete (id) VALUES (?)").run(
          experience_id,
        );
      }
    }

    if (experience.hasOwnProperty("endless")) {
      if (!experience.endless) {
        db.prepare("DELETE FROM endless WHERE id = ?").run(experience_id);
      } else {
        db.prepare("INSERT OR IGNORE INTO endless (id) VALUES (?)").run(
          experience_id,
        );
      }
    }
  }
}

export async function newEntry(request) {
  const trans = db.transaction((data) => {
    const entry_id = db
      .prepare("INSERT INTO entry (name, body) VALUES (?, ?)")
      .run(data.name, data.body).lastInsertRowid;

    editEntryMeta(entry_id, data);

    return entry_id;
  });

  const data = await request.json();

  return trans(data);
}
