import db from "$lib/server/database";

/* Renders internal links with syntax:
 * [[title]] -> <a href="url/for/title">title</a>
 * [[display|title]] -> <a href="url/for/title">display</a>
 */
export function markdownItInternalLink(md) {
  md.inline.ruler.push("internal_link", (state, silent) => {
    const pos = state.pos;

    if (
      !silent &&
      state.src.charCodeAt(pos) === 0x5b /* [ */ &&
      state.src.charCodeAt(pos + 1) === 0x5b /* [ */
    ) {
      const start = pos + 2,
        end = state.src.indexOf("]]", start);

      if (end !== -1) {
        const content = state.src.slice(start, end),
          parts = content.split("|"),
          show = parts[0],
          link = parts.length === 2 ? parts[1] : show,
          id = db
            .prepare(
              `
              SELECT id
              FROM entry
              WHERE name = ?
              `,
            )
            .pluck()
            .get(link);

        if (typeof id !== "undefined") {
          const tokenOpen = state.push("link_open", "a", 1);
          tokenOpen.attrs = [["href", `/entries/${id}`]];
          tokenOpen.markup = "[[";
          tokenOpen.position = pos;
          tokenOpen.size = 2;

          const tokenContent = state.push("text", "", 0);
          tokenContent.content = show;
          tokenContent.position = pos + 2;
          tokenContent.size = show.length;

          const tokenClose = state.push("link_close", "a", -1);
          tokenClose.markup = "]]";
          tokenClose.position = end;
          tokenClose.size = 2;

          state.pos = end + 2;

          return true;
        }
      }
    }

    return false;
  });
}

// turn --- into an em-dash
export function markdownItDash(md) {
  md.inline.ruler.push("dash", (state, silent) => {
    const pos = state.pos;

    if (
      !silent &&
      state.src.charCodeAt(pos) === 0x2d /* - */ &&
      state.src.charCodeAt(pos + 1) === 0x2d /* - */ &&
      state.src.charCodeAt(pos + 2) === 0x2d /* - */
    ) {
      const token = state.push("text", "", 0);
      token.markup = "---";
      token.content = "—"; // em-dash
      token.position = pos;
      token.size = 3;

      state.pos = pos + 3;

      return true;
    }

    return false;
  });
}
