import Database from "better-sqlite3";
import getRenderer from "$lib/server/markdown";
import { dateify } from "$lib/server/datetime";

export const db = new Database("data/stories.db");
export default db;

// TODO can use VIEWs for some of these

export function all_collections() {
  return db
    .prepare(
      `
      SELECT
        e.name
      FROM entry AS e
      INNER JOIN collection AS c ON c.id = e.id
      ORDER BY e.name
      `,
    )
    .pluck()
    .all();
}

export function all_years() {
  return db
    .prepare(
      `
      SELECT
        name
      FROM year
      ORDER BY name
      `,
    )
    .pluck()
    .all();
}

export function init_tags() {
  return db
    .prepare(
      `
      SELECT name FROM taxonomy AS t
      INNER JOIN manual_taxonomy AS m ON m.id = t.id
      `,
    )
    .pluck()
    .all()
    .reduce((a, t) => ({ ...a, [t]: [] }), {});
}

export function all_tags() {
  const tags = init_tags();

  const tmp = db
    .prepare(
      `
      SELECT
        a.name AS tax,
        b.name AS tag
      FROM taxonomy AS a
      INNER JOIN manual_taxonomy AS d ON d.id = a.id
      INNER JOIN tag AS b ON b.taxonomy_id = a.id
      ORDER BY b.name
      `,
    )
    .all();

  for (const { tax, tag } of tmp) {
    tags[tax].push(tag);
  }

  return tags;
}

export function getBasicEntry(entry_id) {
  return db
    .prepare(
      `
      SELECT
        e.name,
        e.body,
        ry.name AS version_year,
        oy.name AS original_year,
        IIF(c.id IS NULL, 0, 1) AS collection
      FROM entry AS e
      LEFT OUTER JOIN collection AS c ON c.id = e.id
      LEFT OUTER JOIN version_release AS r ON r.id = e.id
      LEFT OUTER JOIN year AS ry ON ry.id = r.year_id
      LEFT OUTER JOIN original_release AS o ON o.id = e.id
      LEFT OUTER JOIN year AS oy ON oy.id = o.year_id
      WHERE e.id = ?
      `,
    )
    .get(entry_id);
}

export function processCommonFields(entry, kind) {
  const md = getRenderer();

  // here we change entry using call-by-sharing
  entry.body = md.render(entry.body);

  for (const date of ["added", "edited"]) {
    entry[date] = dateify(entry[date]);
  }
  // end here

  const aliases = db
    .prepare(`SELECT name FROM ${kind}_alias WHERE ${kind}_id =?`)
    .all(entry.id);

  const links = db
    .prepare(`SELECT url FROM ${kind}_link WHERE ${kind}_id = ?`)
    .all(entry.id);

  for (const i in links) {
    const url = new URL(links[i].url);
    links[i].url = url.href;
    links[i].text = url.hostname;
  }

  return { aliases, links };
}
