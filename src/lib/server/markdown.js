import MarkdownIt from "markdown-it";
import MarkdownitFootnote from "markdown-it-footnote";
import markdownitDeflist from "markdown-it-deflist";
import {
  markdownItInternalLink,
  markdownItDash,
} from "$lib/server/markdown-it-plugins";

export default function getRenderer() {
  return new MarkdownIt()
    .use(MarkdownitFootnote) // pandoc-style footnotes https://pandoc.org/MANUAL.html#footnotes
    .use(markdownitDeflist) // pandoc-style <dl>s https://pandoc.org/MANUAL.html#definition-lists
    .use(markdownItDash) // --- -> em-dash
    .use(markdownItInternalLink); // [[x]] -> <a href="/path/to/x">a</a>, [[y|x]] -> <a href="/path/to/x>y</a>
}
