import Database from "better-sqlite3";

export const dbs = new Database("data/session.db");
export default dbs;

export function createSession() {
  const session_id = crypto.randomUUID(); // TODO generate proper id
  return session_id;
}

export function checkAdmin(cookies) {
  const session_id = cookies.get("sessionid");

  dbs
    .prepare(
      `
      UPDATE session
      SET last_activity = UNIXEPOCH()
      WHERE session_id = ?
      `
    )
    .run(session_id);

  const res = dbs
    .prepare(
      `
      SELECT s.id
      FROM session AS s
      INNER JOIN account AS a ON a.id = s.account_id
      WHERE s.session_id = ?
      AND a.username = ?
      `
    )
    .pluck()
    .get(session_id, "admin");

  return res !== undefined;
}
