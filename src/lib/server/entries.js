import { error } from "@sveltejs/kit";
import { dateify } from "$lib/server/datetime";

export const page_default = 1;
export const show_default = 20;

function get_page(data) {
  const pagestr = data.get("page") ?? page_default;

  let page = parseInt(pagestr);

  if (isNaN(page) || page < 1) {
    error(
            422,
            `You sent '${pagestr}' for page, which must be a positive nonzero integer`,
          );
  }

  return page;
}

function get_lim(data) {
  const showstr = data.get("show") ?? show_default;

  let lim = parseInt(showstr);

  if (isNaN(lim) || lim < 1) {
    error(
            422,
            `You sent '${showstr}' for lim, which must be a positive nonzero integer`,
          );
  }

  return lim;
}

export function get_pages(data, max_num, number) {
  let lim = get_lim(data);

  const pages = lim === 0 ? 1 : Math.max(1, Math.ceil(number / lim));

  let page = get_page(data);

  if (page > pages) {
    page = pages;
  }

  const off = (page - 1) * lim;

  return { page, pages, lim, off };
}

export function get_way(data) {
  const ways = ["ASC", "DESC"];

  const way_default = ways[1];

  const way = data.get("way") ?? way_default;
  if (!ways.includes(way)) {
    error(
            422,
            `You sent '${way}' for way, which must be one of ${ways.join(", ")}`,
          );
  }

  return { ways, way };
}

export function get_sort(data, sort_map, default_i) {
  const sort_keys = [...sort_map.keys()];

  const sort_default = sort_keys[default_i];

  const sort = data.get("sort") ?? sort_default;

  if (!sort_keys.includes(sort)) {
    error(
            422,
            `You sent '${sort}' for sort, which must be one of ${sort_keys.join(
              ", ",
            )}`,
          );
  }

  const sort_col = sort_map.get(sort);

  return { sort_keys, sort, sort_col };
}

export function dateify_list(entries, fields) {
  for (const [i, entry] of entries.entries()) {
    for (const date of fields) {
      if (entry[date] !== null) {
        entries[i][date] = dateify(entry[date]);
      }
    }
  }
}
