export function dateString(date) {
  return date.toISOString().slice(0, 10);
}
