const date_options = {
  year: "numeric",
  month: "short",
  day: "numeric",
};

const date_format = new Intl.DateTimeFormat("en-GB", date_options);

const datetime_format = new Intl.DateTimeFormat("en-GB", {
  ...date_options,
  hour: "numeric",
  minute: "numeric",
  hour12: true,
});

export function format_date(date) {
  return date === null ? "-" : date_format.format(date);
}

export function format_datetime(date) {
  return date === null ? "-" : datetime_format.format(date);
}

export function another_day(end, start) {
  return (
    end.getDate() !== start.getDate() ||
    end.getMonth() !== start.getMonth() ||
    end.getYear() !== start.getYear()
  );
}

export function capitalise(string) {
  return string.toLowerCase().replace(/^\w/, (c) => c.toUpperCase());
}
