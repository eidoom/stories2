import { goto } from "$app/navigation";

export async function post({ url, apidata }) {
  // remove the edit page and the original visit of the entry page from the navigation history
  // so that it only shows the current visit of the entry page
  // history.go(-2);
  // but if I navigate somewhere by typing the url, then we go back once too many :/
  // comment out for now

  const response = await fetch(url, {
    method: "POST",
    body: JSON.stringify(apidata),
    headers: {
      "content-type": "application/json",
    },
  });

  if (!response.ok) {
    return { status: response.status, statusText: response.statusText };
  }

  if (response.redirected) {
    goto(response.url);
  }
}

export function add(list, el) {
  return [...list, el];
}

export function del(list, i) {
  return list.filter((_, ii) => ii !== i);
}
