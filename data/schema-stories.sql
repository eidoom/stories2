BEGIN;

-- entries --

-- TODO enforce that collections have unique names
CREATE TABLE IF NOT EXISTS entry (
	id INTEGER NOT NULL PRIMARY KEY,
	name TEXT NOT NULL,
	body TEXT NOT NULL DEFAULT '',
	added INTEGER NOT NULL DEFAULT (UNIXEPOCH()),
	edited INTEGER NOT NULL DEFAULT (UNIXEPOCH())
) STRICT;

CREATE TABLE IF NOT EXISTS entry_alias (
	id INTEGER NOT NULL PRIMARY KEY,
	entry_id INTEGER NOT NULL REFERENCES entry (id) ON DELETE CASCADE,
	name TEXT NOT NULL
) STRICT;

CREATE TABLE IF NOT EXISTS entry_link (
	id INTEGER NOT NULL PRIMARY KEY,
	entry_id INTEGER NOT NULL REFERENCES entry (id) ON DELETE CASCADE,
	url TEXT NOT NULL
) STRICT;

-- collections --

CREATE TABLE IF NOT EXISTS entry_collection (
	id INTEGER NOT NULL PRIMARY KEY,
	entry_id INTEGER NOT NULL REFERENCES entry (id) ON DELETE CASCADE,
	collection_id INTEGER NOT NULL REFERENCES entry (id) ON DELETE RESTRICT
) STRICT;

CREATE TABLE IF NOT EXISTS entry_ordinal (
	id INTEGER NOT NULL PRIMARY KEY REFERENCES entry_collection (id) ON DELETE CASCADE,
	number real NOT NULL
) STRICT;

CREATE TABLE IF NOT EXISTS collection (
	id INTEGER NOT NULL PRIMARY KEY REFERENCES entry (id) ON DELETE CASCADE
) STRICT;

-- currently unused
CREATE TABLE IF NOT EXISTS continuing (
	id INTEGER NOT NULL PRIMARY KEY REFERENCES collection (id) ON DELETE CASCADE
) STRICT;

-- experiences --

CREATE TABLE IF NOT EXISTS experience (
	id INTEGER NOT NULL PRIMARY KEY,
	entry_id INTEGER NOT NULL REFERENCES entry (id) ON DELETE CASCADE
) STRICT;

CREATE TABLE IF NOT EXISTS playpause (
	id INTEGER NOT NULL PRIMARY KEY,
	experience_id INTEGER NOT NULL REFERENCES experience (id) ON DELETE CASCADE,
	timestamp INTEGER NOT NULL DEFAULT (UNIXEPOCH())
) STRICT;

CREATE TABLE IF NOT EXISTS complete (
	id INTEGER NOT NULL PRIMARY KEY REFERENCES experience (id) ON DELETE CASCADE
) STRICT;

-- synonyms: incompletable, pseudoactive, unbounded, boundless, infinite, eternal, constant, perisitent, continual, continuous, perpetual, everlasting, withoutend, neverending, unending, ouroboros, lemniscate
CREATE TABLE IF NOT EXISTS endless (
	id INTEGER NOT NULL PRIMARY KEY REFERENCES experience (id) ON DELETE CASCADE
) STRICT;

-- taxonomies --

CREATE TABLE IF NOT EXISTS taxonomy (
	id INTEGER NOT NULL PRIMARY KEY,
	name TEXT NOT NULL UNIQUE
) STRICT;

CREATE TABLE IF NOT EXISTS manual_taxonomy (
	id INTEGER NOT NULL PRIMARY KEY REFERENCES taxonomy (id) ON DELETE CASCADE
) STRICT;

CREATE TABLE IF NOT EXISTS tag (
	id INTEGER NOT NULL PRIMARY KEY,
	name TEXT NOT NULL,
	taxonomy_id INTEGER NOT NULL REFERENCES taxonomy (id) ON DELETE RESTRICT,
	body TEXT NOT NULL DEFAULT '',
	added INTEGER NOT NULL DEFAULT (UNIXEPOCH()),
	edited INTEGER NOT NULL DEFAULT (UNIXEPOCH()),
	UNIQUE(name, taxonomy_id)
) STRICT;

CREATE TABLE IF NOT EXISTS tag_alias (
	id INTEGER NOT NULL PRIMARY KEY,
	tag_id INTEGER NOT NULL REFERENCES tag (id) ON DELETE CASCADE,
	name TEXT NOT NULL
) STRICT;

CREATE TABLE IF NOT EXISTS tag_link (
	id INTEGER NOT NULL PRIMARY KEY,
	tag_id INTEGER NOT NULL REFERENCES tag (id) ON DELETE CASCADE,
	url TEXT NOT NULL
) STRICT;

CREATE TABLE IF NOT EXISTS entry_tag (
	id INTEGER NOT NULL PRIMARY KEY,
	entry_id INTEGER NOT NULL REFERENCES entry (id) ON DELETE CASCADE,
	tag_id INTEGER NOT NULL REFERENCES tag (id) ON DELETE RESTRICT,
	UNIQUE(entry_id, tag_id)
) STRICT;

-- initially used to organise format by medium
-- use to categorise tags of one taxonomy by tags of another taxonomy
-- or to specify subtags of supertags within the same taxonomy
CREATE TABLE IF NOT EXISTS tag_group (
	id INTEGER NOT NULL PRIMARY KEY,
	member_id INTEGER NOT NULL REFERENCES tag (id) ON DELETE CASCADE,
	group_id INTEGER NOT NULL REFERENCES tag (id) ON DELETE RESTRICT,
	UNIQUE(member_id, group_id)
) STRICT;

-- release years --

CREATE TABLE IF NOT EXISTS year (
	id INTEGER NOT NULL PRIMARY KEY,
	name INTEGER NOT NULL UNIQUE
) STRICT;

-- leave blank if same as original_release
CREATE TABLE IF NOT EXISTS version_release (
	id INTEGER NOT NULL PRIMARY KEY REFERENCES entry (id) ON DELETE CASCADE,
	year_id INTEGER NOT NULL REFERENCES year (id) ON DELETE RESTRICT
) STRICT;

CREATE TABLE IF NOT EXISTS original_release (
	id INTEGER NOT NULL PRIMARY KEY REFERENCES entry (id) ON DELETE CASCADE,
	year_id INTEGER NOT NULL REFERENCES year (id) ON DELETE RESTRICT
) STRICT;

COMMIT;
