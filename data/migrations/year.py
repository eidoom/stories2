#!/usr/bin/env python3

import sqlite3, pathlib, shutil


if __name__ == "__main__":
    db = pathlib.Path("../stories.db")

    bkp = pathlib.Path("../stories.db.bkp")
    shutil.copy2(db, bkp)

    con = sqlite3.connect(db)
    cur = con.cursor()

    schema = pathlib.Path("../schema.sql").read_text()
    cur.executescript(schema)

    cur.execute(
        """
        WITH a AS (
            SELECT
                MIN(y.name) AS name,
                y.id AS year_id,
                ey.entry_id AS entry_id
            FROM year AS y
            INNER JOIN entry_year AS ey ON ey.year_id = y.id
            GROUP BY ey.entry_id
            HAVING COUNT(y.id) = 2
        )
        INSERT INTO original_release (id, year_id)
        SELECT entry_id, year_id
        FROM a
        """
    )

    cur.execute(
        """
        WITH a AS (
            SELECT
                MAX(y.name) AS name,
                y.id AS year_id,
                ey.entry_id AS entry_id
            FROM year AS y
            INNER JOIN entry_year AS ey ON ey.year_id = y.id
            GROUP BY ey.entry_id
            HAVING COUNT(y.id) = 2
        )
        INSERT INTO version_release (id, year_id)
        SELECT entry_id, year_id
        FROM a
        """
    )

    cur.execute(
        """
        INSERT INTO version_release (id, year_id)
        SELECT
            entry_id,
            year_id
        FROM entry_year
        GROUP BY entry_id
        HAVING COUNT(year_id) = 1
        """
    )

    cur.execute(
        """
        DROP TABLE entry_year
        """
    )

    con.commit()
