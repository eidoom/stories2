#!/usr/bin/env python3

import sqlite3, pathlib, shutil


if __name__ == "__main__":
    db = pathlib.Path("../stories.db")

    bkp = pathlib.Path("../stories.db.bkp")
    shutil.copy2(db, bkp)

    con = sqlite3.connect(db)
    cur = con.cursor()

    schema = pathlib.Path("../schema.sql").read_text()
    cur.executescript(schema)

    taxonomies = [
        "artist",
        "format",
        "genre",
        "language",
        "medium",
        "provenance",
        "setting",
    ]

    derived = [
        "medium",
    ]

    for taxonomy in taxonomies:
        if taxonomy not in derived:
            cur.execute(
                f"""
                DELETE FROM {taxonomy} AS t
                WHERE t.id IN (
                    SELECT tt.id FROM {taxonomy} AS tt
                    LEFT OUTER JOIN entry_{taxonomy} AS et ON tt.id = et.{taxonomy}_id
                    WHERE et.id IS NULL
                )
                """
            )

        cur.execute(f"INSERT INTO taxonomy (name) VALUES (?)", (taxonomy,))
        taxonomy_id = cur.lastrowid

        if taxonomy not in derived:
            cur.execute(f"INSERT INTO manual_taxonomy (id) VALUES (?)", (taxonomy_id,))

        try:
            cur.execute(
                f"""
                INSERT INTO tag (name, taxonomy_id)
                SELECT name, ? FROM {taxonomy}
                """,
                (taxonomy_id,),
            )
        except sqlite3.IntegrityError:
            raise Exception(f"Problem with {taxonomy}")

        if taxonomy not in derived:
            cur.execute(
                f"""
                INSERT INTO entry_tag (entry_id, tag_id)
                SELECT et.entry_id, n.id FROM entry_{taxonomy} AS et
                INNER JOIN {taxonomy} AS o ON o.id = et.{taxonomy}_id
                INNER JOIN tag AS n ON n.name = o.name
                WHERE n.taxonomy_id = ?
                """,
                (taxonomy_id,),
            )

    cur.execute(
        f"""
        INSERT INTO tag_group (member_id, group_id)
        SELECT t1.id, t2.id FROM format_medium AS fm
        INNER JOIN format AS f ON fm.format_id = f.id
        INNER JOIN tag AS t1 ON t1.name = f.name
        INNER JOIN taxonomy AS c1 ON c1.id = t1.taxonomy_id
        INNER JOIN medium AS m ON fm.medium_id = m.id
        INNER JOIN tag AS t2 ON t2.name = m.name
        INNER JOIN taxonomy AS c2 ON c2.id = t2.taxonomy_id
        WHERE c1.name = 'format' AND c2.name = 'medium'
        """
    )

    cur.execute(f"DROP TABLE format_medium")
    cur.execute(f"DROP TABLE genre_group")

    for taxonomy in taxonomies:
        cur.execute(f"DROP TABLE {taxonomy}")

        if taxonomy not in derived:
            cur.execute(f"DROP TABLE entry_{taxonomy}")

    con.commit()
