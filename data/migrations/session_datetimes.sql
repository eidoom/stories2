ALTER TABLE session RENAME TO session_old;

.read "schema-session.sql"

BEGIN;

INSERT INTO session (id, session_id, account_id)
    SELECT id, session_id, account_id FROM session_old;

DROP TABLE session_old;

COMMIT;
