#!/usr/bin/env python3

import sqlite3, pathlib, shutil


if __name__ == "__main__":
    db = pathlib.Path("../stories.db")

    bkp = pathlib.Path("../stories.db.bkp")
    shutil.copy2(db, bkp)

    con = sqlite3.connect(db)
    cur = con.cursor()

    schema = pathlib.Path("../schema.sql").read_text()
    cur.executescript(schema)

    classification = {
        "book": [
            "novella",
            "handbook",
            "rulebook",
            "novel",
            "novelette",
            "cookbook",
            "usage dictionary",
            "companion book",
            "sourcebook",
            "collection",
            "short story",
            "triptych",
            "lorebook",
        ],
        "comics": [
            "graphic novel",
            "comic",
            "comics miniseries",
            "comics limited series",
        ],
        "video": [
            "miniseries",
            "film",
            "television",
            "documentary",
        ],
        "web": [
            "digital encyclopædia",
            "wiki",
            "serial",
            "website",
            "essay",
            "blog",
        ],
        "drawing": [
            "artbook",
        ],
        "game": [
            "role-playing game",
            "board game",
            "tabletop",
            "video game",
            "miniature wargame",
            "computer game",
            "interactive fiction",
            "puzzle",
            "card game",
        ],
    }

    # cur.execute(
    #     """
    #     WITH a AS (
    #         SELECT
    #             MIN(y.name) AS name,
    #             y.id AS year_id,
    #             ey.entry_id AS entry_id
    #         FROM year AS y
    #         INNER JOIN entry_year AS ey ON ey.year_id = y.id
    #         GROUP BY ey.entry_id
    #         HAVING COUNT(y.id) = 2
    #     )
    #     INSERT INTO original_release (id, year_id)
    #     SELECT entry_id, year_id
    #     FROM a
    #     """
    # )

    # con.commit()
