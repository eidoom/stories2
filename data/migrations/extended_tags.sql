ALTER TABLE tag RENAME TO tag_old;

-- adds:
--   fields body, added, and edited to tag
--     (can't ADD COLUMN due to default current date)
--   tables tag_link and tag_alias
.read "schema-stories.sql"

BEGIN;

INSERT INTO tag (id, name, taxonomy_id)
    SELECT id, name, taxonomy_id FROM tag_old;

DROP TABLE tag_old;

-- dirty hack so that foreign keys don't end up pointing at tag_old
ALTER TABLE tag RENAME TO tag_old;
ALTER TABLE tag_old rename TO tag;

COMMIT;
