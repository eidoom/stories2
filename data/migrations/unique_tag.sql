ALTER TABLE tag RENAME TO tag_old;
ALTER TABLE entry_tag RENAME TO entry_tag_old;
ALTER TABLE tag_group RENAME TO tag_group_old;

.read "schema-stories.sql"

BEGIN;

INSERT INTO tag (id, name, taxonomy_id)
    SELECT id, name, taxonomy_id FROM tag_old;

DROP TABLE tag_old;

INSERT INTO entry_tag (id, entry_id, tag_id)
    SELECT id, entry_id, tag_id FROM entry_tag_old;

DROP TABLE entry_tag_old;

INSERT INTO tag_group (id, member_id, group_id)
    SELECT id, member_id, group_id FROM tag_group_old;

DROP TABLE tag_group_old;

COMMIT;
