# migrations

Run the Python script

```sh
./script.py
```

or apply the SQL script

```sh
cd ..
cat migrations/script.sql | sqlite3 stories.db
```
