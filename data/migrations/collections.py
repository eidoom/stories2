#!/usr/bin/env python3

import sqlite3, pathlib, shutil


if __name__ == "__main__":
    db = pathlib.Path("../stories.db")

    bkp = pathlib.Path("../stories.db.bkp")
    shutil.copy2(db, bkp)

    con = sqlite3.connect(db)
    cur = con.cursor()

    cur.execute(
        """
        ALTER TABLE entry_collection
        RENAME TO old_entry_collection
        """
    )

    cur.execute(
        """
        ALTER TABLE entry_ordinal
        RENAME TO old_entry_ordinal
        """
    )

    cur.execute(
        """
        ALTER TABLE collection
        RENAME TO old_collection
        """
    )

    schema = pathlib.Path("../schema.sql").read_text()

    cur.executescript(schema)

    cur.execute(
        """
        INSERT INTO entry (name, body, added, edited)
        SELECT name, body, added, edited FROM old_collection
        """
    )

    cur.execute(
        """
        INSERT INTO entry_collection (id, entry_id, collection_id)
        SELECT oec.id, oec.entry_id, nc.id FROM old_entry_collection AS oec
        INNER JOIN old_collection AS oc ON oc.id = oec.collection_id
        INNER JOIN entry AS nc ON (oc.name = nc.name AND oc.body = nc.body AND oc.added = nc.added AND oc.edited = nc.edited)
        """
    )

    cur.execute(
        """
        INSERT INTO collection (id)
        SELECT nc.id FROM old_collection AS oc
        INNER JOIN entry AS nc ON (oc.name = nc.name AND oc.body = nc.body AND oc.added = nc.added AND oc.edited = nc.edited)
        """
    )

    cur.execute(
        """
        INSERT INTO entry_ordinal (id, number)
        SELECT id, number FROM old_entry_ordinal
        """
    )

    cur.execute(
        """
        INSERT INTO entry_link (entry_id, url)
        SELECT nc.id, l.url FROM collection_link AS l
        INNER JOIN old_collection AS oc ON oc.id = l.collection_id
        INNER JOIN entry AS nc ON (oc.name = nc.name AND oc.body = nc.body AND oc.added = nc.added AND oc.edited = nc.edited)
        """
    )

    for table in (
        "old_collection",
        "old_entry_collection",
        "old_entry_ordinal",
        "collection_alias",
        "collection_link",
        "collection_group",
        "collection_ordinal",
    ):
        cur.execute(f"DROP TABLE {table}")

    con.commit()
