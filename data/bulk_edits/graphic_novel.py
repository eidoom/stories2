#!/usr/bin/env python3

import sqlite3

def pluck_factory(_, row):
    return row[0]

if __name__ == "__main__":
    con = sqlite3.connect("../stories.db")
    cur = con.cursor()
    cur.row_factory = pluck_factory

    eids = cur.execute("select e.id from entry as e inner join entry_format as ef on ef.entry_id = e.id inner join format as f on f.id = ef.format_id inner join entry_format as ef2 on ef2.entry_id = e.id inner join format as f2 on f2.id = ef2.format_id group by e.id having f.name <> 'graphic novel' and f2.name = 'comic'").fetchall()

    fid = cur.execute("select id from format where name = 'graphic novel'").fetchone()

    for eid in eids:
        cur.execute("insert into entry_format (entry_id, format_id) values (?, ?)", (eid, fid))

    con.commit()
