#!/usr/bin/env python3

import sqlite3, pathlib, shutil, re


def dict_factory(cursor, row):
    fields = [column[0] for column in cursor.description]
    return {key: value for key, value in zip(fields, row)}


if __name__ == "__main__":
    db = pathlib.Path("../stories.db")

    bkp = pathlib.Path("../stories.db.bkp")
    shutil.copy2(db, bkp)

    con = sqlite3.connect(db)
    con.row_factory = dict_factory
    cur = con.cursor()

    entries = cur.execute(
        "SELECT * FROM entry WHERE body LIKE '%{{ short(%';"
    ).fetchall()

    for entry in entries:
        shorts = entry["body"]
        entry_body = shorts.split("{{", 1)[0].strip()

        genres = [
            r["name"]
            for r in (
                cur.execute(
                    """
                    SELECT t.name
                    FROM entry AS e
                    INNER JOIN entry_tag AS et ON et.entry_id = e.id
                    INNER JOIN tag AS t ON t.id = et.tag_id
                    INNER JOIN taxonomy AS x ON x.id = t.taxonomy_id
                    WHERE e.id = ?
                        AND x.name = 'genre'
                    """,
                    (entry["id"],),
                ).fetchall()
            )
        ]
        if "science fiction" in genres:
            genre = "science fiction"
        elif "fantasy" in genres:
            genre = "fantasy"
        else:
            print(genres)

        artists = [
            r["name"]
            for r in (
                cur.execute(
                    """
                    SELECT t.name
                    FROM entry AS e
                    INNER JOIN entry_tag AS et ON et.entry_id = e.id
                    INNER JOIN tag AS t ON t.id = et.tag_id
                    INNER JOIN taxonomy AS x ON x.id = t.taxonomy_id
                    WHERE e.id = ?
                        AND x.name = 'artist'
                    """,
                    (entry["id"],),
                ).fetchall()
            )
        ]
        if len(artists) == 1:
            artist = artists[0]
        elif "Liu Cixin" in artists:
            artist = "Liu Cixin"
        else:
            print(artists)

        cur.execute(
            "UPDATE entry SET body=?, edited=UNIXEPOCH() WHERE id = ?",
            (entry_body, entry["id"]),
        )

        cur.execute("INSERT INTO collection (id) VALUES (?)", (entry["id"],))

        matches = re.findall(r"{{ short\((.*)\) }}\s*([^{]*)", shorts)

        for short_meta_raw, short_body in matches:
            short_body = short_body.strip()

            short_meta = {
                key: (
                    [subval.strip('" ') for subval in value[1:-1].split(",")]
                    if value[0] == "["
                    else value.strip('"')
                )
                for key, value in re.findall(
                    r'(\w+)=(".*?"|\[.*?\]|\d+),?', short_meta_raw
                )
            }

            extra = [
                key
                for key in short_meta.keys()
                if key
                not in [
                    "title",
                    "link",
                    "year",
                    "years",
                    "started",
                    "finished",
                    "uni",
                    "rest",
                    "num",
                    "alt",
                    "desc",
                ]
            ]
            if extra:
                print(extra)

            if "desc" in short_meta:
                short_body = f"*{short_meta['desc']}*\n\n{short_body}"

            cur.execute(
                """
                INSERT INTO entry (name, body, added)
                VALUES (?, ?, ?)
                """,
                (short_meta["title"], short_body, entry["added"]),
            )
            short_id = cur.lastrowid

            cur.execute(
                """
                INSERT INTO entry_collection (entry_id, collection_id)
                VALUES (?, ?)
                """,
                (short_id, entry["id"]),
            )

            cur.executemany(
                """
                INSERT INTO entry_tag (entry_id, tag_id)
                VALUES (?, (SELECT id FROM tag WHERE name = ?))
                """,
                (
                    (short_id, "short story"),
                    (short_id, artist),
                    (short_id, genre),
                ),
            )

            if "link" in short_meta:
                cur.execute(
                    """
                    INSERT INTO entry_link (entry_id, url)
                    VALUES (?, ?)
                    """,
                    (short_id, short_meta["link"]),
                )

            cur.execute(
                """
                INSERT INTO experience (entry_id)
                VALUES (?)
                """,
                (short_id,),
            )
            xp_id = cur.lastrowid
            cur.execute(
                """
                INSERT INTO complete (id)
                VALUES (?)
                """,
                (xp_id,),
            )

            if "started" in short_meta:
                cur.executemany(
                    """
                    INSERT INTO playpause (experience_id, timestamp)
                    VALUES (?, UNIXEPOCH(?))
                    """,
                    (
                        (xp_id, short_meta["started"]),
                        (
                            xp_id,
                            short_meta["finished"]
                            if "finished" in short_meta
                            else short_meta["started"],
                        ),
                    ),
                )
            # else:
            # didn't record read time, so just mark completed (at unknown time)
            # also duplicate short stories (between collections):
            # 1. Semley's Necklace from The Wind's Twelve Quarters (previously in Rocannon's World)
            # 2. Old Music and the Slave Women from The Birthday of the World and Other Stories (previously in Five Ways to Forgiveness)
            # resolve these manually

            if "alt" in short_meta:
                cur.execute(
                    """
                    INSERT INTO entry_alias (entry_id, name)
                    VALUES (?, ?)
                    """,
                    (short_id, short_meta["alt"]),
                )

            if set(["year", "years"]) & set(short_meta.keys()):
                if "years" in short_meta and len(short_meta["years"]) > 1:
                    year_original = int(min(short_meta["years"]))
                    cur.execute(
                        """
                        INSERT OR IGNORE INTO year (name)
                        VALUES (?)
                        """,
                        (year_original,),
                    )
                    cur.execute(
                        """
                        INSERT INTO original_release (id, year_id)
                        VALUES (?, (SELECT id FROM year WHERE name = ?))
                        """,
                        (short_id, year_original),
                    )

                    year_version = int(max(short_meta["years"]))
                    cur.execute(
                        """
                        INSERT OR IGNORE INTO year (name)
                        VALUES (?)
                        """,
                        (year_version,),
                    )
                    cur.execute(
                        """
                        INSERT INTO version_release (id, year_id)
                        VALUES (?, (SELECT id FROM year WHERE name = ?))
                        """,
                        (short_id, year_version),
                    )
                else:
                    year = int(
                        short_meta["year"]
                        if "year" in short_meta
                        else min(short_meta["years"])
                    )
                    cur.execute(
                        """
                        INSERT OR IGNORE INTO year (name)
                        VALUES (?)
                        """,
                        (year,),
                    )
                    cur.execute(
                        """
                        INSERT INTO original_release (id, year_id)
                        VALUES (?, (SELECT id FROM year WHERE name = ?))
                        """,
                        (short_id, year),
                    )

            if "uni" in short_meta:
                existing = cur.execute(
                    """
                    SELECT id FROM entry WHERE name = ?
                    """,
                    (short_meta["uni"],),
                ).fetchone()
                if existing is None:
                    cur.execute(
                        """
                        INSERT INTO entry (name, added)
                        VALUES (?, ?)
                        """,
                        (short_meta["uni"], entry["added"]),
                    )
                    uni_id = cur.lastrowid
                    cur.execute("INSERT INTO collection (id) VALUES (?)", (uni_id,))
                else:
                    uni_id = existing["id"]

                cur.execute(
                    """
                    INSERT INTO entry_collection (entry_id, collection_id)
                    VALUES (?, ?)
                    """,
                    (short_id, uni_id),
                )
                ecid = cur.lastrowid

                if "num" in short_meta:
                    cur.execute(
                        """
                        INSERT INTO entry_ordinal (id, number)
                        VALUES (?, ?)
                        """,
                        (ecid, float(short_meta["num"])),
                    )

    con.commit()
