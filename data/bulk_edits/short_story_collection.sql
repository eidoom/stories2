BEGIN;

INSERT INTO tag (name, taxonomy_id)
    SELECT 'short story collection', id
    FROM taxonomy
    WHERE name = 'format';

CREATE TEMP TABLE data AS
    SELECT
        e.id AS eid,
        et1.id AS et1id,
        et2.id AS et2id
    FROM entry AS e
    INNER JOIN entry_tag AS et1 ON et1.entry_id = e.id
    INNER JOIN tag AS t1 ON t1.id = et1.tag_id
    INNER JOIN entry_tag AS et2 ON et2.entry_id = e.id
    INNER JOIN tag AS t2 ON t2.id = et2.tag_id
    WHERE
        t1.name = 'collection'
        AND
        t2.name = 'short story';

INSERT INTO entry_tag (entry_id, tag_id)
    SELECT
        eid,
        (SELECT id FROM tag WHERE name = 'short story collection')
    FROM data;

DELETE FROM entry_tag
    WHERE id IN ( SELECT (et1id) FROM data );

DELETE FROM entry_tag
    WHERE id IN ( SELECT (et2id) FROM data );

COMMIT;
