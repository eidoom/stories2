BEGIN;

CREATE TEMP TABLE to_move AS
    SELECT v.id, v.year_id FROM version_release AS v
    LEFT OUTER JOIN original_release AS o ON o.id = v.id
    WHERE o.year_id IS NULL;

INSERT INTO original_release (id, year_id) SELECT id, year_id FROM to_move;

DELETE FROM version_release WHERE id IN ( SELECT (id) FROM to_move );

COMMIT;
